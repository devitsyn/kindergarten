SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';


CREATE SCHEMA IF NOT EXISTS `kindergarten` DEFAULT CHARACTER SET utf8 ;
USE `kindergarten` ;

-- -----------------------------------------------------
-- Table `kindergarten`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `kindergarten`.`user` (
  `iduser` INT NOT NULL AUTO_INCREMENT,
  `fio` VARCHAR(150) NOT NULL,
  `group` VARCHAR(150) NOT NULL,
  `login` VARCHAR(150) NOT NULL,
  `password` VARCHAR(150) NOT NULL,
  PRIMARY KEY (`iduser`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `kindergarten`.`report`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `kindergarten`.`report` (
  `idreport` INT NOT NULL AUTO_INCREMENT,
  `date` DATE NULL,
  `report` VARCHAR(150) NULL,
  `iduser` INT NOT NULL,
  PRIMARY KEY (`idreport`),
  INDEX `fk_report_user_idx` (`iduser` ASC) VISIBLE,
  CONSTRAINT `fk_report_user`
    FOREIGN KEY (`iduser`)
    REFERENCES `kindergarten`.`user` (`iduser`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
