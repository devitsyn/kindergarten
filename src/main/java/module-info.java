module com.devitsyn.kindergarten {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires junit;

    opens com.devitsyn.kindergarten to javafx.fxml;
    exports com.devitsyn.kindergarten;
}
