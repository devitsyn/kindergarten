package com.devitsyn.kindergarten;

import java.io.IOException;
import java.sql.SQLException;

import com.devitsyn.kindergarten.db.DatabaseHandler;
import com.devitsyn.kindergarten.db.User;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class SignUpController {

    DatabaseHandler dbHandler = new DatabaseHandler();
    Alert alert = new Alert(Alert.AlertType.INFORMATION);

    @FXML
    private TextField groupName;

    @FXML
    private TextField login_field;

    @FXML
    private PasswordField password_field;

    @FXML
    private TextField signUpName;

    @FXML
    private void switchToStart() throws IOException {
        App.setRoot("start");
    }

    @FXML
    private void signIn() {
        signUpNewUser();
    }

    private void signUpNewUser() {
        dbHandler = new DatabaseHandler();
        alert = new Alert(Alert.AlertType.INFORMATION);

        boolean fioIsEmpty = signUpName == null || signUpName.getText().trim().length() == 0;
        boolean groupIsEmpty = groupName == null || groupName.getText().trim().length() == 0;
        boolean loginIsEmpty = login_field == null || login_field.getText().trim().length() == 0;
        boolean passwordIsEmpty = password_field == null || password_field.getText().trim().length() == 0;

        alert.setTitle("Информация о регистрации");
        alert.setHeaderText(null);

        if (loginIsEmpty || passwordIsEmpty || fioIsEmpty || groupIsEmpty) {

            alert.setContentText("Информация не заполнена");
            alert.showAndWait();

        } else if (login_field.getText().equals("admin")){

            alert.setContentText("Нельзя зарегистрировать пользователя с логином admin");
            alert.showAndWait();

        } else {
            String fio = signUpName.getText();
            String group = groupName.getText();
            String login = login_field.getText();
            String password = password_field.getText();

            User user = new User("",fio, group, login, password);

            dbHandler.signUpUser(user);

            alert.setContentText("Регистрация прошла успешно");
            alert.showAndWait();

            login_field.clear();
            password_field.clear();
            signUpName.clear();
            groupName.clear();
        }
    }
}
