package com.devitsyn.kindergarten;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import com.devitsyn.kindergarten.db.DatabaseHandler;
import com.devitsyn.kindergarten.db.User;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class StartController {

    @FXML
    private Button authSigInButton;

    @FXML
    private TextField login_field;

    @FXML
    private PasswordField password_field;

    @FXML
    private void switchToSignUp() throws IOException {
        App.setRoot("signUp");
    }

    @FXML
    void initialize() {
        authSigInButton.setOnAction(event -> {

            String loginText = login_field.getText().trim();
            String loginPassword = password_field.getText().trim();
            if(loginText.equals("admin") && loginPassword.equals("admin")){
                try {
                    App.setRoot("admin");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else if(!loginText.equals("") && !loginPassword.equals("")) {
                try {
                    loginUser(loginText, loginPassword);
                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else
                System.out.println("Пустое поле для входа и пароля");
        });
    }

    private void loginUser(String loginText, String loginPassword) throws SQLException, ClassNotFoundException, IOException {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        DatabaseHandler dbHandler = new DatabaseHandler();

        User user = dbHandler.signIn(loginText,loginPassword);

        if (!User.instance().getLogin().equals("")) {
            App.setRoot("main");
        }
    }
}
