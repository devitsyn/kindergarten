package com.devitsyn.kindergarten;

import javafx.beans.property.SimpleStringProperty;

import java.sql.Date;

public class TableRowMain {
        SimpleStringProperty date;
        SimpleStringProperty report;

        public TableRowMain(String date, String report){ // Строка для таблицы на вкладке Main
            this.date = new SimpleStringProperty(date);
            this.report = new SimpleStringProperty(report);
        }

        public String getDate() {
            return date.get();
        }

        public void setDate(String id) {
            this.date.set(id);
        }

        public String getReport() {
            return report.get();
        }

        public void setReport(String name) {
            this.report.set(name);
        }
}

