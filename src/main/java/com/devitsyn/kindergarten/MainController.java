package com.devitsyn.kindergarten;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

import com.devitsyn.kindergarten.db.DatabaseHandler;
import com.devitsyn.kindergarten.db.User;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

public class MainController {

    @FXML
    private Button addButton;

    @FXML
    private DatePicker dateField;

    @FXML
    private TextField reportField;

    @FXML
    private TableView<TableRowMain> table;

    TableRowMain buffer;
    DatabaseHandler db = new DatabaseHandler();

    public void fillTable(){    //Метод для заполнения таблицы

        ObservableList<TableRowMain> orderRow = db.fillMainTable();  //Получение данных из БД

        table.getItems().clear();
        table.getColumns().clear();     //Очищение таблицы
        table.getItems().setAll(orderRow);  //Заполнение таблицы

        // столбец для вывода date
        TableColumn<TableRowMain, String> dateColumn = new TableColumn<TableRowMain, String>("Дата");
        dateColumn.setCellValueFactory(new PropertyValueFactory<TableRowMain, String>("date"));
        dateColumn.setPrefWidth(70);
        table.getColumns().add(dateColumn);

        // столбец для вывода Наименование
        TableColumn<TableRowMain, String> reportColumn = new TableColumn<TableRowMain, String>("Отчет");
        reportColumn.setCellValueFactory(new PropertyValueFactory<TableRowMain, String>("report"));
        reportColumn.setPrefWidth(453);
        table.getColumns().add(reportColumn);

        TableView.TableViewSelectionModel<TableRowMain> selectionModel = table.getSelectionModel();
        selectionModel.selectedItemProperty().addListener(new ChangeListener<TableRowMain>(){
            public void changed(ObservableValue<? extends TableRowMain> val, TableRowMain oldVal, TableRowMain newVal){
                buffer = newVal;    //Получение выбранного элемента
            }
        });
    }

    @FXML
    private void switchToStart() throws IOException {
        App.setRoot("start");
    }

    @FXML
    void initialize() {
        fillTable();

        addButton.setOnAction(event -> {
            LocalDate value = dateField.getValue();
            String report = reportField.getText();
            db.addReport(String.valueOf(value),report);

            fillTable();
            System.out.println(User.instance().getId());
        });
    }
}
