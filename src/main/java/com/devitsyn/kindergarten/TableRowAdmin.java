package com.devitsyn.kindergarten;

import javafx.beans.property.SimpleStringProperty;

public class TableRowAdmin {
    SimpleStringProperty fio;
    SimpleStringProperty group;
    SimpleStringProperty login;
    SimpleStringProperty password;

    public TableRowAdmin(String fio, String group,String login, String password ){ // Строка для таблицы на вкладке Main
        this.fio = new SimpleStringProperty(fio);
        this.group = new SimpleStringProperty(group);
        this.login = new SimpleStringProperty(login);
        this.password = new SimpleStringProperty(password);

    }

    public String getLogin() {
        return login.get();
    }

    public String getPassword() {
        return password.get();
    }

    public void setPassword(String password) {
        this.password.set(password);
    }

    public void setLogin(String login) {
        this.login.set(login);
    }

    public String getFio() {
        return fio.get();
    }

    public void setFio(String id) {
        this.fio.set(id);
    }

    public String getGroup() {
        return group.get();
    }

    public void setGroup(String name) {
        this.group.set(name);
    }
}