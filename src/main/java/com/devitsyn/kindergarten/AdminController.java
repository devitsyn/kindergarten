package com.devitsyn.kindergarten;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.devitsyn.kindergarten.db.DatabaseHandler;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class AdminController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button backButton;

    @FXML
    private TableView<TableRowAdmin> table;

    TableRowAdmin buffer;
    DatabaseHandler db = new DatabaseHandler();

    public void fillTable(){    //Метод для заполнения таблицы

        ObservableList<TableRowAdmin> orderRow = db.fillAdminTable();  //Получение данных из БД

        table.getItems().clear();
        table.getColumns().clear();     //Очищение таблицы
        table.getItems().setAll(orderRow);  //Заполнение таблицы

        // столбец для вывода fio
        TableColumn<TableRowAdmin, String> fioColumn = new TableColumn<TableRowAdmin, String>("ФИО");
        fioColumn.setCellValueFactory(new PropertyValueFactory<TableRowAdmin, String>("fio"));
        fioColumn.setPrefWidth(200);
        table.getColumns().add(fioColumn);

        // столбец для вывода group
        TableColumn<TableRowAdmin, String> groupColumn = new TableColumn<TableRowAdmin, String>("Группа");
        groupColumn.setCellValueFactory(new PropertyValueFactory<TableRowAdmin, String>("group"));
        groupColumn.setPrefWidth(100);
        table.getColumns().add(groupColumn);

        // столбец для вывода login
        TableColumn<TableRowAdmin, String> loginColumn = new TableColumn<TableRowAdmin, String>("Логин");
        loginColumn.setCellValueFactory(new PropertyValueFactory<TableRowAdmin, String>("login"));
        loginColumn.setPrefWidth(195);
        table.getColumns().add(loginColumn);

        // столбец для вывода password
        TableColumn<TableRowAdmin, String> passwordColumn = new TableColumn<TableRowAdmin, String>("Пароль");
        passwordColumn.setCellValueFactory(new PropertyValueFactory<TableRowAdmin, String>("password"));
        passwordColumn.setPrefWidth(195);
        table.getColumns().add(passwordColumn);

        TableView.TableViewSelectionModel<TableRowAdmin> selectionModel = table.getSelectionModel();
        selectionModel.selectedItemProperty().addListener(new ChangeListener<TableRowAdmin>(){
            public void changed(ObservableValue<? extends TableRowAdmin> val, TableRowAdmin oldVal, TableRowAdmin newVal){
                buffer = newVal;    //Получение выбранного элемента
            }
        });
    }

    @FXML
    private void switchToStart() throws IOException {
        App.setRoot("start");
    }

    @FXML
    void initialize() {
        fillTable();
    }
}
