package com.devitsyn.kindergarten.db;

public class Const {
    public static final String USER_TABLE    = "user";
    public static final String USER_ID       = "iduser";
    public static final String USER_FIO      = "fio";
    public static final String USER_GROUP    = "group";
    public static final String USER_LOGIN    = "login";
    public static final String USER_PASSWORD = "password";

    public static final String REPORT_TABLE   = "report";
    public static final String REPORT_DATE    = "date";
    public static final String REPORT_REPORT  = "report";
    public static final String REPORT_USER    = "iduser";
}
