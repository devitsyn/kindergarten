package com.devitsyn.kindergarten.db;

public class User {
    private String id;
    private String fio;
    private String group;
    private String login;
    private String password;
    public static User user;

    public User(String id,String fio, String group, String login, String password) {
        this.id = id;
        this.fio = fio;
        this.group = group;
        this.login = login;
        this.password = password;
    }

    public static User instance(){ //Получение объекта employee
        user = new User("","","","","");
        return user;
    }

    //Объявление объекта employee
    public static void init(String id, String fio, String group, String login, String password){
        user = new User(id,fio,group,login,password);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String name) {
        this.fio = name;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}