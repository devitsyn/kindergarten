package com.devitsyn.kindergarten.db;

import com.devitsyn.kindergarten.TableRowAdmin;
import com.devitsyn.kindergarten.TableRowMain;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;

public class DatabaseHandler extends Configs {
    Connection dbConnection;
    PreparedStatement prSt;

    public Connection getDbConnection()
            throws ClassNotFoundException, SQLException {
        String connectionString = "jdbc:mysql://" + dbHost + ":" + dbPort + "/" + dbName;

        Class.forName("com.mysql.cj.jdbc.Driver");

        dbConnection = DriverManager.getConnection(connectionString, dbUser, dbPass);

        return dbConnection;
    }

    //Аутентификация
    public User signIn(String login, String password) {
        ResultSet rs = null;

        String select = "SELECT * FROM user WHERE login = \"" + login + "\" AND password = \"" + password + "\";";

        try {
            PreparedStatement prSt = getDbConnection().prepareStatement(select);
            rs = prSt.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            while (rs.next()) {
                User.init(String.valueOf(rs.getInt(1)),rs.getString(2),rs.getString(3),
                        rs.getString(4),rs.getString(5));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return User.instance();
    }

    //Регистрация
    public void signUpUser(User user) {

        String select = "INSERT INTO user VALUES( NULL , \"" + user.getFio() + "\", \"" + user.getGroup() + "\", \"" + user.getLogin() + "\", \"" + user.getPassword() + "\" );";

        try {
            PreparedStatement prSt = getDbConnection().prepareStatement(select);
            prSt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    //Получение данных из БД для заполнения таблицы на вкладке Букеты
    public ObservableList<TableRowMain> fillMainTable() {
        ResultSet rs = null;
        ObservableList<TableRowMain> rowSet = FXCollections.observableArrayList();

        String select = "SELECT * FROM report WHERE iduser = " + User.instance().getId() + " ; ";

        try {
            PreparedStatement prSt = getDbConnection().prepareStatement(select);
            rs = prSt.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            while (rs.next()) {
                rowSet.add( new TableRowMain(rs.getString(2),rs.getString(3)));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return rowSet;
    }

    public ObservableList<TableRowAdmin> fillAdminTable() {
        ResultSet rs = null;
        ObservableList<TableRowAdmin> rowSet = FXCollections.observableArrayList();

        String select = "SELECT * FROM user ; ";

        try {
            PreparedStatement prSt = getDbConnection().prepareStatement(select);
            rs = prSt.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            while (rs.next()) {
                rowSet.add( new TableRowAdmin(rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5)));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return rowSet;
    }

    public void addReport(String date,String report){

        String insert = "INSERT INTO report VALUES( NULL , \"" + date+ "\", \"" + report+ "\", \"" + User.instance().getId()+ "\" );";

        try {
            PreparedStatement prSt = getDbConnection().prepareStatement(insert);
            prSt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
